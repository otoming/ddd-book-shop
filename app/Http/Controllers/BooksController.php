<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Projection\Book\BookFinder;

class BooksController extends Controller
{
    /**
     * @var BookFinder
     */
    private $bookFinder;

    public function __construct(BookFinder $bookFinder)
    {
        $this->bookFinder = $bookFinder;
    }

    public function get(Request $request): \Illuminate\Http\JsonResponse
    {
        $books = $this->bookFinder->findAll();

        return response()->json($books);
    }
}

<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Projection\User\UserFinder;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * @var UserFinder
     */
    private $userFinder;

    public function __construct(UserFinder $userFinder)
    {
        $this->userFinder = $userFinder;
    }

    public function get(Request $request, $userId): \Illuminate\Http\JsonResponse
    {
        $user = $this->userFinder->findById($userId);

        return response()->json($user);
    }
}

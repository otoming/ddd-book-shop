<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Projection\Book\BookFinder;

class OrdersController extends Controller
{
    /**
     * @var BookFinder
     */
    private $bookFinder;

    public function __construct(BookFinder $bookFinder)
    {
        $this->bookFinder = $bookFinder;
    }

    public function get(Request $request, $userId): \Illuminate\Http\JsonResponse
    {
        $orders = $this->bookFinder->findByAssigneeId($userId);

        return response()->json($orders);
    }
}

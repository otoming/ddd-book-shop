<?php


declare(strict_types=1);

namespace App\Infrastructure\Repository;

use Prooph\EventSourcing\Aggregate\AggregateRepository;
use App\Model\Book\Book;
use App\Model\Book\BookId;
use App\Model\Book\BookStore;

final class EventStoreBookStore extends AggregateRepository implements BookStore
{
    public function save(Book $book): void
    {
        $this->saveAggregateRoot($book);
    }

    public function get(BookId $bookId): ?Book
    {
        return $this->getAggregateRoot($bookId->toString());
    }
}

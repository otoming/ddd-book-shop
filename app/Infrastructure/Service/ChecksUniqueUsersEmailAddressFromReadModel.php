<?php


declare(strict_types=1);

namespace App\Infrastructure\Service;

use App\Model\User\EmailAddress;
use App\Model\User\Service\ChecksUniqueUsersEmailAddress;
use App\Model\User\UserId;
use App\Projection\User\UserFinder;

final class ChecksUniqueUsersEmailAddressFromReadModel implements ChecksUniqueUsersEmailAddress
{
    /**
     * @var UserFinder
     */
    private $userFinder;

    public function __construct(UserFinder $userFinder)
    {
        $this->userFinder = $userFinder;
    }

    public function __invoke(EmailAddress $emailAddress): ?UserId
    {
        if ($user = $this->userFinder->findOneByEmailAddress($emailAddress->toString())) {
            return UserId::fromString($user->id);
        }

        return null;
    }
}

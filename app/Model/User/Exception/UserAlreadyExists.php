<?php


declare(strict_types=1);

namespace App\Model\User\Exception;

use App\Model\User\UserId;

final class UserAlreadyExists extends \InvalidArgumentException
{
    public static function withUserId(UserId $userId): UserAlreadyExists
    {
        return new self(sprintf('User with id %s already exists.', $userId->toString()));
    }
}

<?php


declare(strict_types=1);

namespace App\Model\User\Exception;

final class InvalidName extends \InvalidArgumentException
{
    public static function reason(string $msg): InvalidName
    {
        return new self('Invalid user name because ' . $msg);
    }
}

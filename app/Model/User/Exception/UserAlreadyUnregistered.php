<?php


declare(strict_types=1);

namespace App\Model\User\Exception;

use App\Model\User\UserId;

final class UserAlreadyUnregistered extends \InvalidArgumentException
{
    public static function withUserId(UserId $userId): UserAlreadyUnregistered
    {
        return new self(sprintf('User with id %s already unregistered.', $userId->toString()));
    }
}

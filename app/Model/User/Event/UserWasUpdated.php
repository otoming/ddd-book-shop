<?php


declare(strict_types=1);

namespace App\Model\User\Event;

use Prooph\EventSourcing\AggregateChanged;
use App\Model\User\UserId;
use App\Model\User\UserName;

final class UserWasUpdated extends AggregateChanged
{
    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var UserName
     */
    private $username;

    public static function withData(UserId $userId, UserName $name): UserWasUpdated
    {
        /** @var self $event */
        $event = self::occur($userId->toString(), [
            'id' => $userId->toString(),
            'name' => $name->toString()
        ]);

        $event->userId = $userId;
        $event->username = $name;

        return $event;
    }

    public function userId(): UserId
    {
        if (null === $this->userId) {
            $this->userId = UserId::fromString($this->aggregateId());
        }

        return $this->userId;
    }

    public function name(): UserName
    {
        if (null === $this->username) {
            $this->username = UserName::fromString($this->payload['name']);
        }

        return $this->username;
    }

}

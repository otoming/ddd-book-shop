<?php


declare(strict_types=1);

namespace App\Model\User\Event;

use Prooph\EventSourcing\AggregateChanged;
use App\Model\User\UserId;

final class UserWasUnRegistered extends AggregateChanged
{
    /**
     * @var UserId
     */
    private $userId;

    public static function withData(UserId $userId): UserWasUnRegistered
    {
        /** @var self $event */
        $event = self::occur($userId->toString(), [
            'id' => $userId->toString(),
        ]);

        $event->userId = $userId;

        return $event;
    }

    public function userId(): UserId
    {
        if (null === $this->userId) {
            $this->userId = UserId::fromString($this->aggregateId());
        }

        return $this->userId;
    }

}

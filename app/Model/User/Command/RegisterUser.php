<?php


declare(strict_types=1);

namespace App\Model\User\Command;

use Assert\Assertion;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use App\Model\User\EmailAddress;
use App\Model\User\UserId;
use App\Model\User\UserName;
use Zend\Validator\EmailAddress as EmailAddressValidator;

final class RegisterUser extends Command implements PayloadConstructable
{
    use PayloadTrait;

    public static function withData(string $name, string $email): RegisterUser
    {
        return new self([
            'name' => $name,
            'email' => $email,
        ]);
    }

    public function userId(): UserId
    {
        return UserId::generate();
    }

    public function name(): UserName
    {
        return UserName::fromString($this->payload['name']);
    }

    public function emailAddress(): EmailAddress
    {
        return EmailAddress::fromString($this->payload['email']);
    }

    protected function setPayload(array $payload): void
    {
        Assertion::keyExists($payload, 'name');
        Assertion::string($payload['name']);
        Assertion::keyExists($payload, 'email');
        $validator = new EmailAddressValidator();
        Assertion::true($validator->isValid($payload['email']));

        $this->payload = $payload;
    }
}

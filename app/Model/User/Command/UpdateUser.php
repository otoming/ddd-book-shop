<?php


declare(strict_types=1);

namespace App\Model\User\Command;

use Assert\Assertion;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use App\Model\User\UserId;
use App\Model\User\UserName;

final class UpdateUser extends Command implements PayloadConstructable
{
    use PayloadTrait;

    public static function withData(string $userId, string $name): UpdateUser
    {
        return new self([
            'user_id' => $userId,
            'name' => $name,
        ]);
    }

    public function userId(): UserId
    {
        return UserId::fromString($this->payload['user_id']);
    }

    public function name(): UserName
    {
        return UserName::fromString($this->payload['name']);
    }

    protected function setPayload(array $payload): void
    {
        Assertion::keyExists($payload, 'user_id');
        Assertion::uuid($payload['user_id']);
        Assertion::keyExists($payload, 'name');
        Assertion::string($payload['name']);

        $this->payload = $payload;
    }
}

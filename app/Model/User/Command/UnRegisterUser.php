<?php


declare(strict_types=1);

namespace App\Model\User\Command;

use Assert\Assertion;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use App\Model\User\UserId;

final class UnRegisterUser extends Command implements PayloadConstructable
{
    use PayloadTrait;

    public static function withData(string $userId): UnRegisterUser
    {
        return new self([
            'user_id' => $userId
        ]);
    }

    public function userId(): UserId
    {
        return UserId::fromString($this->payload['user_id']);
    }

    protected function setPayload(array $payload): void
    {
        Assertion::keyExists($payload, 'user_id');
        Assertion::uuid($payload['user_id']);

        $this->payload = $payload;
    }
}

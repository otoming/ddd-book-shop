<?php


declare(strict_types=1);

namespace App\Model\User;

use Prooph\EventSourcing\AggregateChanged;
use Prooph\EventSourcing\AggregateRoot;
use App\Model\Entity;
use App\Model\Book\Book;
use App\Model\Book\BookId;
use App\Model\User\Event\UserWasRegistered;
use App\Model\User\Event\UserWasUnRegistered;
use App\Model\User\Event\UserWasUpdated;
use App\Model\User\Event\UserWasRegisteredAgain;

final class User extends AggregateRoot implements Entity
{
    /**
     * @var UserId
     */
    private $userId;

    /**
     * @var UserName
     */
    private $name;

    /**
     * @var EmailAddress
     */
    private $emailAddress;

    public static function registerWithData(
        UserId $userId,
        UserName $name,
        EmailAddress $emailAddress
    ): User {
        $self = new self();

        $self->recordThat(UserWasRegistered::withData($userId, $name, $emailAddress));

        return $self;
    }


    public function unRegister(): void
    {
        $this->recordThat(UserWasUnRegistered::withData($this->userId));
    }

    public function registerAgain(UserName $name): void
    {
        $this->recordThat(UserWasRegisteredAgain::withData($this->userId, $name, $this->emailAddress));
    }

    public function update(UserName $name): void
    {
        $this->recordThat(UserWasUpdated::withData($this->userId, $name));
    }

    public function userId(): UserId
    {
        return $this->userId;
    }

    public function name(): UserName
    {
        return $this->name;
    }

    public function emailAddress(): EmailAddress
    {
        return $this->emailAddress;
    }

    protected function aggregateId(): string
    {
        return $this->userId->toString();
    }

    protected function whenUserWasRegistered(UserWasRegistered $event): void
    {
        $this->userId = $event->userId();
        $this->name = $event->name();
        $this->emailAddress = $event->emailAddress();
    }

    protected function whenUserWasUnRegistered(UserWasUnRegistered $event): void
    {
        $this->userId = $event->userId();
    }

    protected function whenUserWasUpdated(UserWasUpdated $event): void
    {
        $this->userId = $event->userId();
        $this->name = $event->name();
    }

    protected function whenUserWasRegisteredAgain(UserWasRegisteredAgain $event): void
    {
    }

    public function sameIdentityAs(Entity $other): bool
    {
        return get_class($this) === get_class($other) && $this->userId->sameValueAs($other->userId);
    }

    /**
     * Apply given event
     */
    protected function apply(AggregateChanged $e): void
    {
        $handler = $this->determineEventHandlerMethodFor($e);

        if (! method_exists($this, $handler)) {
            throw new \RuntimeException(sprintf(
                'Missing event handler method %s for aggregate root %s',
                $handler,
                get_class($this)
            ));
        }

        $this->{$handler}($e);
    }

    protected function determineEventHandlerMethodFor(AggregateChanged $e): string
    {
        return 'when' . implode(array_slice(explode('\\', get_class($e)), -1));
    }
}

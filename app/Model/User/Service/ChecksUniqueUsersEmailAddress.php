<?php


declare(strict_types=1);

namespace App\Model\User\Service;

use App\Model\User\EmailAddress;
use App\Model\User\UserId;

interface ChecksUniqueUsersEmailAddress
{
    public function __invoke(EmailAddress $emailAddress): ?UserId;
}

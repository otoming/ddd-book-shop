<?php


declare(strict_types=1);

namespace App\Model\User\Handler;

use App\Model\User\Command\RegisterUser;
use App\Model\User\Exception\UserAlreadyExists;
use App\Model\User\Exception\UserNotFound;
use App\Model\User\Service\ChecksUniqueUsersEmailAddress;
use App\Model\User\User;
use App\Model\User\UserCollection;

class RegisterUserHandler
{
    /**
     * @var UserCollection
     */
    private $userCollection;

    /**
     * @var ChecksUniqueUsersEmailAddress
     */
    private $checksUniqueUsersEmailAddress;

    public function __construct(
        UserCollection $userCollection,
        ChecksUniqueUsersEmailAddress $checksUniqueUsersEmailAddress
    ) {
        $this->userCollection = $userCollection;
        $this->checksUniqueUsersEmailAddress = $checksUniqueUsersEmailAddress;
    }

    public function __invoke(RegisterUser $command): void
    {
        if ($userId = ($this->checksUniqueUsersEmailAddress)($command->emailAddress())) {
            if (! $user = $this->userCollection->get($userId)) {
                throw UserNotFound::withUserId($userId);
            }

            $user->registerAgain($command->name());
        } else {
            $userId = $command->userId();
            if ($user = $this->userCollection->get($userId)) {
                throw UserAlreadyExists::withUserId($userId);
            }
            $user = User::registerWithData($userId, $command->name(), $command->emailAddress());
        }

        $this->userCollection->save($user);
    }
}

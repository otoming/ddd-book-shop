<?php


declare(strict_types=1);

namespace App\Model\User\Handler;

use App\Model\User\Query\GetAllUsers;
use App\Projection\User\UserFinder;
use React\Promise\Deferred;

class GetAllUsersHandler
{
    private $userFinder;

    public function __construct(UserFinder $userFinder)
    {
        $this->userFinder = $userFinder;
    }

    public function __invoke(GetAllUsers $query, Deferred $deferred = null)
    {
        $user = $this->userFinder->findAll();
        if (null === $deferred) {
            return $user;
        }

        $deferred->resolve($user);
    }
}

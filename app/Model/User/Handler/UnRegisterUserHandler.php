<?php


declare(strict_types=1);

namespace App\Model\User\Handler;

use App\Model\User\Command\UnRegisterUser;
use App\Model\User\Exception\UserAlreadyUnregistered;
use App\Model\User\Exception\UserNotFound;
use App\Model\User\UserCollection;
use App\Projection\User\UserFinder;

class UnRegisterUserHandler
{
    /**
     * @var UserCollection
     */
    private $userCollection;

    private $userFinder;

    public function __construct(
        UserCollection $userCollection,
        UserFinder $userFinder
    ) {
        $this->userCollection = $userCollection;
        $this->userFinder = $userFinder;
    }

    public function __invoke(UnRegisterUser $command): void
    {
        $userId = $command->userId();

        if (! $user = $this->userCollection->get($userId)) {
            throw UserNotFound::withUserId($userId);
        }
        elseif (null === $this->userFinder->findById($userId->toString())){
            throw UserAlreadyUnregistered::withUserId($userId);
        }
        else{
            $user->unRegister();
        }

        $this->userCollection->save($user);

    }
}

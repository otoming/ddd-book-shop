<?php


declare(strict_types=1);

namespace App\Model\User\Handler;

use App\Model\User\Command\UpdateUser;
use App\Model\User\Exception\UserNotFound;
use App\Model\User\UserCollection;

class UpdateUserHandler
{
    /**
     * @var UserCollection
     */
    private $userCollection;

    public function __construct(
        UserCollection $userCollection
    ) {
        $this->userCollection = $userCollection;
    }

    public function __invoke(UpdateUser $command): void
    {
        $userId = $command->userId();

        if (! $user = $this->userCollection->get($userId)) {
            throw UserNotFound::withUserId($userId);
        }
        else{
            $user->update($command->name());
        }

        $this->userCollection->save($user);

    }
}

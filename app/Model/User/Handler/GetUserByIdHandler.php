<?php


declare(strict_types=1);

namespace App\Model\User\Handler;

use App\Model\User\Query\GetUserById;
use App\Projection\User\UserFinder;
use React\Promise\Deferred;

class GetUserByIdHandler
{
    private $userFinder;

    public function __construct(UserFinder $userFinder)
    {
        $this->userFinder = $userFinder;
    }

    public function __invoke(GetUserById $query, Deferred $deferred = null)
    {
        $user = $this->userFinder->findById($query->userId());
        if (null === $deferred) {
            return $user;
        }

        $deferred->resolve($user);
    }
}

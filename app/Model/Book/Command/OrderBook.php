<?php


declare(strict_types=1);

namespace App\Model\Book\Command;

use Assert\Assertion;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use App\Model\Book\BookId;
use App\Model\User\UserId;

final class OrderBook extends Command implements PayloadConstructable
{
    use PayloadTrait;

    public static function with(string $assigneeId,string $bookId): OrderBook
    {
        return new self([
            'assignee_id' => $assigneeId,
            'book_id' => $bookId,
        ]);
    }

    public function bookId(): BookId
    {
        return BookId::fromString($this->payload['book_id']);
    }

    public function assigneeId(): UserId
    {
        return UserId::fromString($this->payload['assignee_id']);
    }

    protected function setPayload(array $payload): void
    {
        Assertion::keyExists($payload, 'assignee_id');
        Assertion::uuid($payload['assignee_id']);
        Assertion::keyExists($payload, 'book_id');
        Assertion::uuid($payload['book_id']);

        $this->payload = $payload;
    }
}

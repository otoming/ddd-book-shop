<?php


declare(strict_types=1);

namespace App\Model\Book\Command;

use App\Model\Book\BookStatus;
use Assert\Assertion;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use App\Model\Book\BookId;
use App\Model\User\UserId;

final class AddBook extends Command implements PayloadConstructable
{
    use PayloadTrait;

    public static function with(): AddBook
    {
        return new self([]);
    }

    public function bookId(): BookId
    {
        return BookId::generate();
    }

    public function status(): BookStatus
    {
        return BookStatus::AVAILABLE();
    }

    protected function setPayload(array $payload): void
    {
        $this->payload = $payload;
    }
}

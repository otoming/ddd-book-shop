<?php


declare(strict_types=1);

namespace App\Model\Book;

interface BookStore
{
    public function save(Book $book): void;

    public function get(BookId $bookId): ?Book;
}

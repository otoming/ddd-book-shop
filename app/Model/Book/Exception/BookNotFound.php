<?php


declare(strict_types=1);

namespace App\Model\Book\Exception;

use App\Model\Book\BookId;

final class BookNotFound extends \InvalidArgumentException
{
    public static function withBookId(BookId $bookId): BookNotFound
    {
        return new self(sprintf('Book with id %s cannot be found.', $bookId->toString()));
    }
}

<?php


declare(strict_types=1);

namespace App\Model\Book\Exception;

use App\Model\Book\Book;
use App\Model\Book\BookStatus;

final class BookNotAvailable extends \RuntimeException
{
    public static function triedStatus(BookStatus $status, Book $book): BookNotAvailable
    {
        return new self(sprintf(
            'Tried to change status of Book %s to %s. But Book is not marked as available!',
            $book->bookId()->toString(),
            $status->toString()
        ));
    }
}

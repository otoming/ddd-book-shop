<?php


declare(strict_types=1);

namespace App\Model\Book;

use App\Model\Enum;

/**
 * @method static BookStatus AVAILABLE()
 * @method static BookStatus PURCHASED()
 */
final class BookStatus extends Enum
{
    public const AVAILABLE = 'available';
    public const PURCHASED = 'purchased';
}

<?php


declare(strict_types=1);

namespace App\Model\Book\Event;

use Prooph\EventSourcing\AggregateChanged;
use App\Model\Book\BookId;
use App\Model\Book\BookStatus;
use App\Model\User\UserId;

final class BookWasOrdered extends AggregateChanged
{
    /**
     * @var BookId
     */
    private $bookId;

    /**
     * @var BookStatus
     */
    private $oldStatus;

    /**
     * @var BookStatus
     */
    private $newStatus;

    /**
     * @var UserId
     */
    private $assigneeId;

    public static function fromStatus(BookId $bookId, BookStatus $oldStatus, BookStatus $newStatus, UserId $assigneeId): BookWasOrdered
    {
        /** @var self $event */
        $event = self::occur($bookId->toString(), [
            'old_status' => $oldStatus->toString(),
            'new_status' => $newStatus->toString(),
            'assignee_id' => $assigneeId->toString(),
        ]);

        $event->bookId = $bookId;
        $event->oldStatus = $oldStatus;
        $event->newStatus = $newStatus;
        $event->assigneeId = $assigneeId;

        return $event;
    }

    public function bookId(): BookId
    {
        if (null === $this->bookId) {
            $this->bookId = BookId::fromString($this->aggregateId());
        }

        return $this->bookId;
    }

    public function oldStatus(): BookStatus
    {
        if (null === $this->oldStatus) {
            $this->oldStatus = BookStatus::byName($this->payload['old_status']);
        }

        return $this->oldStatus;
    }

    public function newStatus(): BookStatus
    {
        if (null === $this->newStatus) {
            $this->newStatus = BookStatus::byName($this->payload['new_status']);
        }

        return $this->newStatus;
    }

    public function assigneeId(): UserId
    {
        if (null === $this->assigneeId) {
            $this->assigneeId = UserId::fromString($this->payload['assignee_id']);
        }

        return $this->assigneeId;
    }
}

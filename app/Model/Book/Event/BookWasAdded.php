<?php


declare(strict_types=1);

namespace App\Model\Book\Event;

use Prooph\EventSourcing\AggregateChanged;
use App\Model\Book\BookId;
use App\Model\Book\BookStatus;
use App\Model\User\UserId;

final class BookWasAdded extends AggregateChanged
{
    /**
     * @var BookId
     */
    private $bookId;

    /**
     * @var BookStatus
     */
    private $status;

    public static function withData(BookId $bookId, BookStatus $status): BookWasAdded
    {
        /** @var self $event */
        $event = self::occur($bookId->toString(), [
            'status' => $status->toString(),
        ]);

        $event->bookId = $bookId;
        $event->status = $status;

        return $event;
    }

    public function bookId(): BookId
    {
        if (null === $this->bookId) {
            $this->bookId = BookId::fromString($this->aggregateId());
        }

        return $this->bookId;
    }

    public function status(): BookStatus
    {
        if (null === $this->status) {
            $this->status = BookStatus::byName($this->payload['status']);
        }

        return $this->status;
    }
}

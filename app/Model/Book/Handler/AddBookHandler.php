<?php


declare(strict_types=1);

namespace App\Model\Book\Handler;

use App\Model\Book\Command\AddBook;
use App\Model\Book\BookStore;
use App\Model\Book\Book;

class AddBookHandler
{
    /**
     * @var BookStore
     */
    private $bookStore;

    public function __construct(BookStore $bookStore)
    {
        $this->bookStore = $bookStore;
    }

    public function __invoke(AddBook $command): void
    {
        $book = Book::addWithData($command->bookId(), $command->status());

        $this->bookStore->save($book);
    }
}

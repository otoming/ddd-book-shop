<?php


declare(strict_types=1);

namespace App\Model\Book\Handler;

use App\Model\Book\Command\OrderBook;
use App\Model\Book\Exception\BookNotFound;
use App\Model\Book\BookStore;
use App\Model\User\Exception\UserNotFound;
use App\Model\User\UserCollection;

class OrderBookHandler
{
    /**
     * @var BookStore
     */
    private $bookStore;

    /**
     * @var UserCollection
     */
    private $userCollection;

    public function __construct(BookStore $bookStore, UserCollection $userCollection)
    {
        $this->bookStore = $bookStore;
        $this->userCollection = $userCollection;
    }

    public function __invoke(OrderBook $command): void
    {
        $book = $this->bookStore->get($command->bookId());

        if (! $book) {
            throw BookNotFound::withBookId($command->bookId());
        }

        $user = $this->userCollection->get($command->assigneeId());

        if (! $user) {
            throw UserNotFound::withUserId($command->assigneeId());
        }

        $book->purchase($command->assigneeId());

        $this->bookStore->save($book);
    }
}

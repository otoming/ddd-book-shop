<?php


declare(strict_types=1);

namespace App\Model\Book;

use App\Model\Book\Event\BookWasAdded;
use Prooph\EventSourcing\AggregateChanged;
use Prooph\EventSourcing\AggregateRoot;
use App\Model\Entity;
use App\Model\Book\Event\BookWasOrdered;
use App\Model\User\UserId;

final class Book extends AggregateRoot implements Entity
{
    /**
     * @var BookId
     */
    private $bookId;

    /**
     * @var UserId
     */
    private $assigneeId;

    /**
     * @var BookStatus
     */
    private $status;

    public static function addWithData(
        BookId $bookId,
        BookStatus $status
    ): Book {
        $self = new self();

        $self->recordThat(BookWasAdded::withData($bookId, $status));

        return $self;
    }

    /**
     * @param $assignedId
     * @throws Exception\BookNotAvailable
     */
    public function purchase($assignedId): void
    {
        $status = BookStatus::PURCHASED();

        if (! $this->status->is(BookStatus::AVAILABLE())) {
            throw Exception\BookNotAvailable::triedStatus($status, $this);
        }

        $this->recordThat(BookWasOrdered::fromStatus($this->bookId, $this->status, $status, $assignedId));
    }

    public function bookId(): BookId
    {
        return $this->bookId;
    }

    public function assigneeId(): UserId
    {
        return $this->assigneeId;
    }

    public function status(): BookStatus
    {
        return $this->status;
    }

    protected function whenBookWasAdded(BookWasAdded $event): void
    {
        $this->bookId = $event->bookId();
        $this->status = $event->status();
    }

    protected function whenBookWasOrdered(BookWasOrdered $event): void
    {
        $this->assigneeId = $event->assigneeId();
        $this->status = $event->newStatus();
    }

    protected function aggregateId(): string
    {
        return $this->bookId->toString();
    }

    public function sameIdentityAs(Entity $other): bool
    {
        return get_class($this) === get_class($other) && $this->bookId->sameValueAs($other->bookId);
    }

    /**
     * Apply given event
     */
    protected function apply(AggregateChanged $e): void
    {
        $handler = $this->determineEventHandlerMethodFor($e);

        if (! method_exists($this, $handler)) {
            throw new \RuntimeException(sprintf(
                'Missing event handler method %s for aggregate root %s',
                $handler,
                get_class($this)
            ));
        }

        $this->{$handler}($e);
    }

    protected function determineEventHandlerMethodFor(AggregateChanged $e): string
    {
        return 'when' . implode(array_slice(explode('\\', get_class($e)), -1));
    }
}

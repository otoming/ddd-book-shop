<?php


declare(strict_types=1);

namespace App\Model\Book\Query;

final class GetBookById
{
    /**
     * @var string
     */
    private $bookId;

    public function __construct(string $bookId)
    {
        $this->bookId = $bookId;
    }

    public function bookId(): string
    {
        return $this->bookId;
    }
}

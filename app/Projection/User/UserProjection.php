<?php


declare(strict_types=1);

namespace App\Projection\User;


use Camuthig\EventStore\Package\Projection\ReadModelProjection;
use Prooph\EventStore\Projection\ReadModelProjector;
use App\Model\Book\Event\BookWasOrdered;
use App\Model\User\Event\UserWasRegistered;
use App\Model\User\Event\UserWasUnRegistered;
use App\Model\User\Event\UserWasUpdated;

/**
 * Class UserProjection
 * @package App\Projection\User
 */
final class UserProjection implements ReadModelProjection
{
    public function project(ReadModelProjector $projector): ReadModelProjector
    {
        $projector->fromStream('event_stream')
            ->when([
                UserWasRegistered::class => function ($state, UserWasRegistered $event) {
                    /** @var UserReadModel $readModel */
                    $readModel = $this->readModel();
                    $readModel->stack('insert', [
                        'id' => $event->userId()->toString(),
                        'name' => $event->name()->toString(),
                        'email' => $event->emailAddress()->toString(),
                    ]);
                },
                UserWasUnRegistered::class => function ($state, UserWasUnRegistered $event) {
                    /** @var UserReadModel $readModel */
                    $readModel = $this->readModel();
                    $readModel->stack('remove', [
                        'id' => $event->userId()->toString(),
                    ]);
                },
                UserWasUpdated::class => function ($state, UserWasUpdated $event) {
                    /** @var UserReadModel $readModel */
                    $readModel = $this->readModel();
                    $readModel->stack('update',
                        [
                            'name' => $event->name()->toString(),
                        ],
                        [
                            'id' => $event->userId()->toString(),
                        ]);
                },
            ]);

        return $projector;
    }
}

<?php


declare(strict_types=1);

namespace App\Projection\Book;

use App\Model\Book\Event\BookWasAdded;
use Camuthig\EventStore\Package\Projection\ReadModelProjection;
use Prooph\EventStore\Projection\ReadModelProjector;
use App\Model\Book\Event\BookWasOrdered;

final class BookProjection implements ReadModelProjection
{

    public function project(ReadModelProjector $projector): ReadModelProjector
    {
        $projector->fromStream('event_stream')
            ->when([
                BookWasAdded::class => function ($state, BookWasAdded $event) {
                    /** @var BookReadModel $readModel */
                    $readModel = $this->readModel();
                    $readModel->stack(
                        'insert',
                        [
                            'id' => $event->bookId()->toString(),
                            'status' => $event->status()->toString(),
                        ]
                    );
                },
                BookWasOrdered::class => function ($state, BookWasOrdered $event) {
                    /** @var BookReadModel $readModel */
                    $readModel = $this->readModel();
                    $readModel->stack(
                        'update',
                        [
                            'status' => $event->newStatus()->toString(),
                            'assignee_id' => $event->assigneeId()->toString(),
                        ],
                        [
                            'id' => $event->bookId()->toString(),
                        ]
                    );
                },
            ]);

        return $projector;
    }
}

<?php


declare(strict_types=1);

namespace App\Projection\Book;

use Doctrine\DBAL\Connection;
use App\Model\Book\BookStatus;
use App\Projection\Table;

class BookFinder
{
    /**
     * @var Connection
     */
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
        $this->connection->setFetchMode(\PDO::FETCH_OBJ);
    }

    public function findAll(): array
    {
        return $this->connection->fetchAll(sprintf('SELECT * FROM %s', Table::BOOK));
    }

    public function findAllAvailable(): array
    {
        return $this->connection->fetchAll(sprintf("SELECT * FROM %s WHERE status = '%s'", Table::BOOK, BookStatus::AVAILABLE));
    }

    public function findByAssigneeId(string $assigneeId): array
    {
        return $this->connection->fetchAll(
            sprintf('SELECT * FROM %s WHERE assignee_id = :assignee_id', Table::BOOK),
            ['assignee_id' => $assigneeId]
        );
    }

    public function findById(string $bookId): ?\stdClass
    {
        $stmt = $this->connection->prepare(sprintf('SELECT * FROM %s where id = :book_id', Table::BOOK));
        $stmt->bindValue('book_id', $bookId);
        $stmt->execute();

        $result = $stmt->fetch();

        if (false === $result) {
            return null;
        }

        return $result;
    }
}

<?php


declare(strict_types=1);

namespace App\Projection;

final class Table
{
    const USER = 'read_user';
    const BOOK = 'read_book';
    const BOOK_ORDER = 'read_book_order';
}

# Book Shop
This setup contains;

 - PHP-FPM (PHP 7)
 - Nginx web server
 - MySQL database

## Run
Make sure your have composer and [Docker](https://docs.docker.com/) installed

Clone the repo

    git clone https://gitlab.com/otoming/ddd-book-shop.git

 Change directory

    cd ddd-book-shop
  Install dependencies

    composer install
  Build and run the Docker containers

    docker-compose up -d  

You should be able to visit your app at http://localhost:8080

Update the .env to point to your database (copy from .env.example)

Run migrations
```bash

docker-compose exec app php artisan migrate
docker-compose exec app php artisan event-store:event-store:create-stream mysql
```

To stop the containers run `docker-compose kill`, and to remove them run `docker-compose rm`


## Run the Projections

Run each of the below commands in a terminal. In a production-like environment,
you would want to run each using a tool like supervisor to keep the process
alive.

```bash
docker-compose exec app php artisan event-store:projection:run user_projection
docker-compose exec app php artisan event-store:projection:run book_projection
```

## Endpoints:

Register user:
```bash
curl -d "{\"email\":\"email@gmail.com\",\"name\":\"test\"}" -X POST http://localhost:8080/api/commands/register-user
```

Unregister (remove) user:
```bash
curl -d "{\"user_id\":\"%USER_UUID%\"}" -X DELETE http://localhost:8080/api/commands/unregister-user
```

Update user:
```bash
curl -d "{\"user_id\":\"%USER_UUID%\",\"name\":\"test\"}" -X PUT http://localhost:8080/api/commands/update-user
```

Get profile:
```bash
curl -X GET http://localhost:8080/api/get-user/%USER_UUID%
```

Get books:
```bash
curl -X GET http://localhost:8080/api/get-books
```

Add book:
```bash
curl -d "{}" -X POST http://localhost:8080/api/commands/add-book
```

Order book:
```bash
curl -d "{\"assignee_id\":\"%USER_UUID%\",\"book_id\":\"%BOOK_UUID%\"}" -X POST http://localhost:8080/api/commands/order-book
```

Get orders:
```bash
curl -X GET http://localhost:8080/api/get-orders/%USER_UUID%
```

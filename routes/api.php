<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['command_name'])->group(function () {
    Route::post('/commands/register-user', [
        'as' => 'command::register-user',
        'uses' => 'ApiCommandController@postAction'
    ]);

    Route::delete('/commands/unregister-user', [
        'as' => 'command::unregister-user',
        'uses' => 'ApiCommandController@postAction'
    ]);

    Route::put('/commands/update-user', [
        'as' => 'command::update-user',
        'uses' => 'ApiCommandController@postAction'
    ]);

    Route::post('/commands/add-book', [
        'as' => 'command::add-book',
        'uses' => 'ApiCommandController@postAction'
    ]);

    Route::post('/commands/order-book', [
        'as' => 'command::order-book',
        'uses' => 'ApiCommandController@postAction'
    ]);

});

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('api/get-user/{userId}', [
    'as' => 'page::get-user',
    'uses' => 'ProfileController@get'
]);

Route::get('api/get-books', [
    'as' => 'page::get-books',
    'uses' => 'BooksController@get'
]);

Route::get('api/get-orders/{userId}', [
    'as' => 'page::get-orders',
    'uses' => 'OrdersController@get'
]);
